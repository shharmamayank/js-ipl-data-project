const fs = require("fs")
const path = require("path");
const { parse } = require("csv-parse");

function wonTossAndMatchBoth() {
    let matches = []
    fs.createReadStream(path.join(__dirname, "../data/matches.csv")).pipe(parse({ columns: true })).on("data", (data) => {
        matches.push(data)
    }).on("end", () => {
        let TossAndMatchBothWinner = matches.reduce((previousValue, currentValue) => {
            // console.log(previousValue)
            if (currentValue.toss_winner == currentValue.winner) {
                // console.log(previousValue)
                if (currentValue.toss_winner in previousValue) {
                    previousValue[currentValue.toss_winner] += 1
                } else {
                   
                    previousValue[currentValue.toss_winner] = 1
                   
                }

            }
            return previousValue
        }, {})
        fs.writeFile(path.join(__dirname, "../public/output/5-won-toss-and-match-both.json"), JSON.stringify(TossAndMatchBothWinner), (error) => {
            if (error !== null) {
                console.log(error);
            }
        })
    })
}
wonTossAndMatchBoth()