const { parse } = require("csv-parse")
const fs = require("fs");
const path = require("path")

function extraRunConceded() {
    let ExtraRuns = []
    fs.createReadStream(path.join(__dirname, "../data/deliveries.csv")).pipe(parse({ columns: true })).on("data", (data) => {
        ExtraRuns.push(data)
    }).on("end", () => {
        let TotalExtraRunPerTeam = ExtraRuns.reduce((previous, current) => {
            if (previous[current.bowling_team] !== undefined) {
                previous[current.bowling_team] += +current.extra_runs
            } else {
                previous[current.bowling_team] = +current.extra_runs
            }
            return previous
        },{})
        fs.writeFile(path.join(__dirname, "../public/output/2-extra-run-conceded.json"),
            JSON.stringify(TotalExtraRunPerTeam),
            (error) => {
                if (error !== null) {
                    console.log(error)
                }

            })

    })


}
(extraRunConceded())