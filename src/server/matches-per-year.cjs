const { parse } = require("csv-parse")
const fs = require("fs")
const path = require("path");

function matchesPerYear() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true }))
        .on('data', (data) => {
            matches.push(data)
            // console.log(matches.season)
        })
        .on("end", () => {
            let TotalMatchesPlayed = matches.reduce((previous, current) => {
                if (current.season in previous) {
                    previous[current.season] += 1
                } else {
                    previous[current.season] = 1
                }
                return previous
            }, {})
            fs.writeFile(
                path.join(__dirname, "../public/output/1-matches-per-year.json"),
                JSON.stringify(TotalMatchesPlayed),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
}
matchesPerYear();
