const path = require("path")
const fs = require("fs")
const { parse } = require("csv-parse")

function HighestPlayerofTheMatchAward() {
    let matches = []
    let deliveries = []
    fs.createReadStream(path.join(__dirname, "../data/matches.csv")).pipe(parse({ columns: true })).on("data", (data) => {
        matches.push(data)
    }).on("end", () => {
        fs.createReadStream(path.join(__dirname, "../data/deliveries.csv")).pipe(parse({ columns: true })).on("data", (data) => {
            deliveries.push(data)
        })
    }).on("end", () => {
        let TotalPlayerOfthematchAwardToEach = matches.reduce((previous, current) => {
            if (current.season in previous) {
                if (current.player_of_match in previous[current.season]) {
                    previous[current.season][current.player_of_match] += 1
                } else {
                    previous[current.season][current.player_of_match] = 1
                
                }
            } else {
                previous[current.season] = {}
                previous[current.season][current.player_of_match] = 1
            }
            return previous
        }, {})
        let ResultObject = {};
        for (let keys in TotalPlayerOfthematchAwardToEach) {
            let GetThehighestWinner = Object.entries(TotalPlayerOfthematchAwardToEach[keys]).slice(0, 1).sort((a, b) => {
                return b[1] - a[1]
            })
            ResultObject[keys] = GetThehighestWinner
        }
        fs.writeFile(path.join(__dirname, "../public/output/3-highest-player-of-match.json"),
            JSON.stringify(ResultObject),
            (error) => {
                if (error !== null) {
                    console.log(error)
                }
            }
        )
    })
}
HighestPlayerofTheMatchAward()
// matches.reduce((accu,match)=>{
//     if (match.season in accu) {
//         if (match.player_of_match in accu[match.season]) {
//             accu[match.season][match.player_of_match] += 1;
//         } else {
//             accu[match.season][match.player_of_match] = 1;
//         }
//     } else {
//         accu[match.season] = {};
//         accu[match.season][match.player_of_match] = 1;
//     }
//     return accu;
// },{})
